var datastore = require('../');

var dsConn = datastore({
	transport: 'http',
	url: 'http://127.0.0.1:8005',
	connect: function() { console.log("connected") },
	request: function(path, request, callback) {
		callback(null, "REQOK");
		console.log(path, request);
	}
});

dsConn.request('/api/ping2', function(error, response, persistent) {
	console.log(error, response, persistent);
});

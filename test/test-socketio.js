var datastore = require('../');

var dsConn = datastore({
	transport: 'socketio',
	url: 'http://127.0.0.1:8005',
	connect: function(reconnected) { console.log("connected", reconnected) },
	request: function(path, request, callback) {
		callback(null, "REQOK");
		console.log(path, request);
	}
});

dsConn.request('/api/ping2', function(error, response, persistent) {
	console.log(error, response);
});

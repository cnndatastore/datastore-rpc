# CNN DataStore JavaScript Client #
Works on browser or Node.js

## Loading ##

### Browser ###

	<script src="datastore-rpc/datastore.min.js"></script>
    
### Node.js ###

    var Datastore = require('datastore-rpc');
    
## Usage ##
    
	var DatastoreRPC = require('./lib/DatastoreRPC');
	var VirtualLink = require('./node_modules/node-network/lib/VirtualLink.js');
	
	var datastoreRPC1 = DatastoreRPC({address: 'dsnyc1'});
	var datastoreRPC2 = DatastoreRPC();
	var rpc1rpc2Link = VirtualLink();
	
	datastoreRPC1.addConnection(rpc1rpc2Link.connection1);
	datastoreRPC2.addConnection(rpc1rpc2Link.connection2);
	
	rpc1rpc2Link.connection1.connect();
	rpc1rpc2Link.connection2.connect();
	
	datastoreRPC1.router.use('/path', function(request, respond, next) {
		console.log("Router 1 request");
		respond("error", "response");
		respond("error2", "response2");
	});
	
	datastoreRPC2.router.use(function(request, respond, next) {
		console.log("Router 2 request");
		respond(null, "received");
	});
	
	datastoreRPC1.once('connect', function() {
		datastoreRPC2.request('dsnyc1', '/path', {key: "val"}, {multipleResponses: true}, function(error, response) {
			console.log("RESPONSE", error, response);
		});
	
		datastoreRPC1.request('dsnyc1-1', '/path', {key: "val"}, {multipleResponses: true}, function(error, response) {
			console.log("RESPONSE", error, response);
		});
	});